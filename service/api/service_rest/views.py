from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Service, AutoVO, Technician

# Create your views here.
class AutoVODetailEncoder(ModelEncoder):
    model = AutoVO
    properties = [
        "import_href",
        "vin",
        "color",
        "year",
    ]

class TechnicianEncoder(ModelEncoder):
    model=Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class ServiceListEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "reason",
        "technician",
        "auto",
        "vip",
        "finished"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "auto": AutoVODetailEncoder(),
    }



class ServiceDetailEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "reason",
        "technician",
        "auto",
        "vip",
        "finished"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "auto": AutoVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"Message": "Could Not Create Technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def detail_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"Message": "Technician Does Not Exist"}
            )
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def list_services(request, auto_vo_id=None):
    if request.method == "GET":
        if auto_vo_id is not None:
            services = Service.objects.filter(auto=auto_vo_id)
        else:
            services = Service.objects.all()

        return JsonResponse(
            {"services": services},
            encoder=ServiceListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            technician_name = content["technician"]
            technician = Technician.objects.get(id=technician_name)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Technician"},
                status=400,
            )
        try:
            auto_href = content["auto"]
            vin = AutoVO.objects.get(import_href=auto_href)
            content["auto"] = vin
        except AutoVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Automobile"},
                status=400,
            )
        if AutoVO.objects.filter(vin=content["vin"]).exists():
            content["vip"] = True
        else:
            content["vip"] = False

        service = Service.objects.create(**content)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def detail_service(request, pk):
    if request.method == "GET":
        try:
            service = Service.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServiceDetailEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse(
                {"Message": "Service Does Not Exist."}
            )
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            service = Service.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceListEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse(
                {"Message": "Service Does Not Exist."}
            )

    else:
        try:
            service = Service.objects.get(id=pk)
            service.finish()

            return JsonResponse(
                service,
                encoder=ServiceDetailEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"Message": "Service Does Not Exist"})
            response.status_code = 404
            return response
