from django.urls import path
from .views import(
    api_sales_people,
    api_sales_person,
    api_potential_customers,
    api_potential_customer,
    api_automobileVOs,
    api_sales_records,
    api_sales_record
)

urlpatterns = [
    path("employees/", api_sales_people, name="api_employees"),
    path("employees/<int:pk>/", api_sales_person, name="api_employee"),
    path("customers/", api_potential_customers, name="api_customers"),
    path("customers/<int:pk>/", api_potential_customer, name="api_customer"),
    path("automobiles/", api_automobileVOs, name="api_automobiles"),
    path("sales/", api_sales_records, name="api_sales"),
    path("sales/<int:pk>/", api_sales_record, name="api_sale"),
]
