import React, { useState, useEffect } from 'react';

const SalesPersonHistory = () => {
    const [sales, setSales] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [search, setSearch] = useState("");
    const [submitted, setSubmitted] = useState(false);


    useEffect(() => {
        const fetchConfig = async () => {
            const salesUrl = 'http://localhost:8090/api/sales/';
            const salesResponse = await fetch(salesUrl);

            if (salesResponse.ok) {
                const salesData = await salesResponse.json();
                setSales(salesData.sales_records);
            }
        };

        fetchConfig();
    }, []);

    const handleSearchChange = async (event) => {
        if (sales && sales.length > 0) {
            const searches = sales.filter((sale) =>
                sale.sales_person.name.includes(search)
            );
            setFiltered(searches);
            setSubmitted(true);
        }
    };


    return(
        <div>
            <div className="mt-3 mb-3">
                <input
                    type="text"
                    className="btn btn-outline-success"
                    value={search}
                    id="form1"
                    onChange={(sal) => setSearch(sal.target.value)}
                />
                <button onClick={handleSearchChange} type="button" className="btn btn-outline-success">
                    Search
                </button>
            </div>
            <h1>Sales Person History</h1>
            {filtered.length > 0 && (
                <table className="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price of Sale</th>
                    </tr>
                </thead>
                <tbody>
                {filtered.map(sale => {
                    return (
                        <tr key={sale.sales_person.id}>
                            <td>{ sale.sales_person.name }</td>
                            <td>{ sale.customer.name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>{ sale.price }</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
            )}
            {submitted && filtered.length === 0 && (
                <div className="alert alert-warning" role="alert">
                    <p>Unavailable Sales Person</p>
                </div>
            )}
        </div>
    );
}

export default SalesPersonHistory;
