import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" style={{ backgroundImage: "linear-gradient(0.25turn, #57CFF2, #EAB9F4, #F2983F)" }}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
            <NavLink className="nav-link active" aria-current="page" to="">Home</NavLink>
            </li>
          </ul>
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item me-auto dropdown me-2">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://img.icons8.com/office/512/carpool.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} /> Cars
              </NavLink>
                <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <NavLink id="new-manufacturer" className="dropdown-item" aria-current="page" to="/manufacturer/new">
                    <img src="https://img.icons8.com/external-phatplus-lineal-color-phatplus/512/external-manufacturer-corporation-phatplus-lineal-color-phatplus.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Manufacturer
                    </NavLink>
                  </li>
                  <li>
                    <NavLink id="new-models" className="dropdown-item" aria-current="page" to="/model/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-data-modelling-data-analytics-flaticons-lineal-color-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Vehicle Model
                    </NavLink>
                  </li>
                  <li>
                    <NavLink id="new-automobile" className="dropdown-item" aria-current="page" to="/automobile/new">
                    <img src="https://img.icons8.com/external-smashingstocks-isometric-smashing-stocks/512/external-car-travel-summer-vacation-smashingstocks-isometric-smashing-stocks-2.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Automobile
                    </NavLink>
                  </li>
                </ul>
              </li>
            <li className="nav-item me-auto dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://img.icons8.com/office/512/support.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} /> Services
              </NavLink>
                <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <NavLink id="new-technician" className="dropdown-item" aria-current="page" to="/technician/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-technician-professions-men-diversity-flaticons-lineal-color-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Technician
                    </NavLink>
                  </li>
                  <li>
                    <NavLink id="new-service" className="dropdown-item" aria-current="page" to="/service/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/2x/external-services-automotive-dealership-flaticons-lineal-color-flat-icons-3.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Service
                    </NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item me-auto dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://img.icons8.com/external-flaticons-flat-flat-icons/512/external-customer-digital-nomading-relocation-flaticons-flat-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} /> Sales
              </NavLink>
                <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <NavLink id="new-record" className="dropdown-item" aria-current="page" to="/record/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-sales-sales-flaticons-lineal-color-flat-icons-3.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Sales Record
                    </NavLink>
                  </li>
                  <li>
                    <NavLink id="new-employee" className="dropdown-item" aria-current="page" to="/employee/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-sales-person-professions-men-diversity-flaticons-lineal-color-flat-icons-2.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Sales Person
                    </NavLink>
                  </li>
                  <li>
                    <NavLink id="new-customer" className="dropdown-item" aria-current="page" to="/customer/new">
                    <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-customers-sewing-flaticons-lineal-color-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
                    New Customer
                    </NavLink>
                  </li>
                </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
