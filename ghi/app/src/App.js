import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ListManufacturer';
import VehicleModelForm from './VehicleModelForm';
import VehicleListModels from './ListVehicleModel';
import AutomobileForm from './AutomobileForm';
import AutomobilesList from './AutomobileList';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './ListTechnician';
import ServiceForm from './ServiceForm';
import ServicesList from './ListService';
import ServicesHistory from './ServiceHistory';
import SalesRecordsForm from './SalesRecordForm';
import SalesPersonForm from './SalesPersonForm';
import SalesCustomerForm from './SalesCustomerForm';
import SalesList from './ListSales';
import PersonSalesList from './ListPersonSales';


function App(props) {
  if (
      props.manufacturers === undefined &&
      props.models === undefined &&
      props.automobiles === undefined &&
      props.technicians === undefined &&
      props.services === undefined &&
      props.sales_records === undefined
    ) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturer">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="manufacturer">
            <Route path="" element={<ManufacturersList manufacturers={props.manufacturers} />} />
          </Route>
          <Route path="model">
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="model">
            <Route path="" element={<VehicleListModels models={props.models} />} />
          </Route>
          <Route path="automobile">
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="automobile">
            <Route path="" element={<AutomobilesList autos={props.autos} />} />
          </Route>
          <Route path="technician">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="technician">
            <Route path="" element={<TechniciansList technicians={props.technicians} />} />
          </Route>
          <Route path="service">
            <Route path="new" element={<ServiceForm />} />
          </Route>
          <Route path="service">
            <Route path="history" element={<ServicesHistory services={props.services} />} />
          </Route>
          <Route path="service">
            <Route path="" element={<ServicesList services={props.services} />} />
          </Route>
          <Route path="record">
            <Route path="new" element={<SalesRecordsForm />} />
          </Route>
          <Route path="record">
            <Route path="" element={<SalesList sales_records={props.sales_records} />} />
          </Route>
          <Route path="record">
            <Route path="employee" element={<PersonSalesList sales_records={props.sales_records} />} />
          </Route>
          <Route path="employee">
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="customer">
            <Route path="new" element={<SalesCustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
