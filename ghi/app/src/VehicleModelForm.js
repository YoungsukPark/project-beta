import React, { useEffect, useState } from 'react';

function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([]);

    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const modelResponse = await fetch(modelUrl, fetchConfig);
        if (modelResponse.ok) {
            const newModel = await modelResponse.json();

            setName('');
            setPictureUrl('');
            setManufacturer('');
            setSubmitted(true);
        }
    }

    const fetchData = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

        const manufacturerResponse = await fetch(manufacturerUrl);

        if (manufacturerResponse.ok) {
            const manufacturerData = await manufacturerResponse.json();
            setManufacturers(manufacturerData.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-vehiclemodel-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" />
                        <label htmlFor="pictureUrl">Picture Url</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleManufacturerChange} value={manufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                        <option value="">Choose a Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return(
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    {submitted && (
                        <div className="alert alert-success mt-3">
                            New Vehicle Model Added
                        </div>
                    )}
                </div>
            </div>
            </div>
        )
    }

export default VehicleModelForm;
