import { Link } from "react-router-dom";

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-3 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management! <br />
        </p>
        <div className="column">
          <div className="card mb-3 shadow">
              <Link to="/manufacturer/" className="btn btn-outline-info">
              <img src="https://img.icons8.com/external-phatplus-lineal-color-phatplus/512/external-manufacturer-corporation-phatplus-lineal-color-phatplus.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Manufacturers!
              </Link>
          </div>
          <div className="card mb-3 shadow">
              <Link to="/model/" className="btn btn-outline-warning">
              <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-data-modelling-data-analytics-flaticons-lineal-color-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Vehicle Models!
              </Link>
          </div>
          <div className="card mb-3 shadow">
              <Link to="/automobile/" className="btn btn-outline-success">
              <img src="https://img.icons8.com/external-smashingstocks-isometric-smashing-stocks/512/external-car-travel-summer-vacation-smashingstocks-isometric-smashing-stocks-2.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Automobiles!
              </Link>
          </div>
          <div className="card mb-3 shadow">
              <Link to="/technician/" className="btn btn-outline-info">
              <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-technician-professions-men-diversity-flaticons-lineal-color-flat-icons.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Technicians!
              </Link>
          </div>
          <div className="card mb-3 shadow">
              <Link to="/service/" className="btn btn-outline-warning">
              <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/2x/external-services-automotive-dealership-flaticons-lineal-color-flat-icons-3.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Services!
              </Link>
          </div>
          <div className="card mb-3 shadow">
              <Link to="/record/" className="btn btn-outline-success">
              <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-sales-sales-flaticons-lineal-color-flat-icons-3.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />
              Click me to see Sales Records!
              </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
