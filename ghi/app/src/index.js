import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadItems() {
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const vehicleModelResponse = await fetch('http://localhost:8100/api/models/');
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const salesListResponse = await fetch('http://localhost:8090/api/sales/');
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
  const serviceResponse = await fetch('http://localhost:8080/api/services/');

  if (
      manufacturerResponse.ok &&
      vehicleModelResponse.ok &&
      automobileResponse.ok &&
      technicianResponse.ok &&
      serviceResponse.ok &&
      salesListResponse.ok
    ) {
        const manufacturerData = await manufacturerResponse.json();
        const vehicleModelData = await vehicleModelResponse.json();
        const automobileData = await automobileResponse.json();
        const technicianData = await technicianResponse.json();
        const serviceData = await serviceResponse.json();
        const salesListData = await salesListResponse.json();
        root.render(
          <React.StrictMode>
            <App
              manufacturers={manufacturerData.manufacturers}
              models={vehicleModelData.models}
              autos={automobileData.autos}
              technicians={technicianData.technicians}
              services={serviceData.services}
              sales_records={salesListData.sales_records}
            />
          </React.StrictMode>
        );
  } else {
    console.error(manufacturerResponse);
    console.error(vehicleModelResponse);
    console.error(automobileResponse);
    console.error(technicianResponse);
    console.error(serviceResponse);
    console.error(salesListResponse);
  }
}

loadItems();
