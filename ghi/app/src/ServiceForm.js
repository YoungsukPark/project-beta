import React, { useEffect, useState } from 'react';


function ServiceForm() {
    const [technicians, setTechnicians] = useState([]);
    const [autos, setAutos] = useState([]);

    const [vin, setVin] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [date, setDate] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');
    const [auto, setAuto] = useState('');
    const [submitted, setSubmitted] = useState(false);


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }
    const handleAutoChange = (event) => {
        const value = event.target.value;
        setAuto(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.customer_name = customerName;
        data.date = date;
        data.reason = reason;
        data.technician = technician;
        data.auto = auto;

        const serviceUrl = 'http://localhost:8080/api/services/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const serviceResponse = await fetch(serviceUrl, fetchConfig);
        if (serviceResponse.ok) {
            const newService = await serviceResponse.json();

            setVin('');
            setCustomerName('');
            setDate('');
            setReason('');
            setTechnician('');
            setAuto('');
            setSubmitted(true);
        }
    }

    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';

        const technicianResponse = await fetch(technicianUrl);
        const autoResponse = await fetch(autoUrl);

        if (technicianResponse.ok && autoResponse.ok) {
            const technicianData = await technicianResponse.json();
            const autoData = await autoResponse.json();
            setTechnicians(technicianData.technicians);
            setAutos(autoData.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Book a New Service</h1>
                    <form onSubmit={handleSubmit} id="create-service-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer Name" required type="text" name="customer-name" id="customer-name" className="form-control" />
                        <label htmlFor="customer-name">Customer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleDateChange} value={date} placeholder="YYYY-MM-DD HH-MM" required type="datetime" name="date" id="date" className="form-control" />
                        <label htmlFor="date">Date Format: YYYY-MM-DD HH:MM</label>
                    </div>
                    <div className="textarea mb-3">
                        <textarea onChange={handleReasonChange} value={reason} name="reason" id="reason" className="form-control"></textarea>
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleTechnicianChange} value={technician} required id="technician" name="technician" className="form-select">
                        <option value="">Choose a Technician</option>
                        {technicians.map(technician => {
                            return(
                                <option key={technician.id} value={technician.id}>
                                    Name: {technician.name} | Employee ID: {technician.employee_number}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleAutoChange} value={auto} required id="auto" name="auto" className="form-select">
                        <option value="">Choose Your VIN, It Must Match the VIN Above.</option>
                        {autos.map(auto => {
                            return(
                                <option key={auto.href} value={auto.href}>
                                    {auto.vin}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    {submitted && (
                        <div className="alert alert-success mt-3">
                            New Service Appointment Made
                        </div>
                    )}
                </div>
            </div>
            </div>
        )
}

export default ServiceForm;
